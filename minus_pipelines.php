<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

function minus_formulaire_charger($flux) {
	if (
		substr($flux['args']['form'], 0, 7) === 'editer_'
		or $flux['args']['form'] === 'construire_formulaire'
	) {
		if (_request('minus_trop_majuscules') ?? false) { // si on a trop de majuscule, on le marque avec un hidden
			$hidden = '<input type="hidden" name="minus_deja_vu_trop_majuscules" value="oui" />';
			if (isset($flux['data']['hidden'])) {
				$flux['data']['_hidden'] .=  $hidden;
			} else {
				$flux['data']['_hidden'] = $hidden;
			}
		}
	}

	return $flux;
}

function minus_formulaire_verifier($flux) {
	if (
		substr($flux['args']['form'], 0, 7) == 'editer_'
		and $flux['args']['args'][0] == 'oui') {// on suppose que les formulaires editer_xxx passse tout oui si on edite pour la première fois
		if (minus_trop_majuscules(_request('titre') ?? '') and !_request('minus_deja_vu_trop_majuscules')){ // si on a trop de majuscules, on l'affiche, sauf si on l'a déjà affiché une fois.
			$flux['data']['titre'] = _T('minus:titre_trop_majuscule');
			set_request('minus_trop_majuscules', true);// On stock le résultat
		}
	}
	return $flux;
}

/**
 * Vérifie les labels des saisies à la construction d'un formulaire
 * @param array
 * @return array;
 **/
function minus_saisies_verifier(array $flux): array {
	// Uniquement à la construire d'un formulaire
	if (_request('formulaire_action') !== 'construire_formulaire') {
		return $flux;
	}
	// Si une personne tient vraiment à faire passer un label avec trop de majuscules
	if (_request('minus_deja_vu_trop_majuscules')) {
		return $flux;
	}

	include_spip('inc/saisies');
	// Trouver la bonne saisie à tester
	foreach ($flux['args']['saisies'] as $cle => $detail) {
		if (
			strpos($cle, 'saisie_modifiee') === 0
			and (
				strpos($cle, '[label]')
				or strpos($cle, '[label_case]')
				or strpos($cle, '[legend]')
			)
		) {
			if (minus_trop_majuscules(saisies_request($cle) ?? '')) {
				$flux['data'][$cle] = _T('minus:label_trop_majuscule');
				set_request('minus_trop_majuscules', true);// On stock le résultat
			}
		}
	}

	return $flux;
}



/**
 * Return true si trop de majuscule dans $txt
 * @param str $txt
 * @return bool
**/
function minus_trop_majuscules(string $txt): bool {
	include_spip('inc/config');
	if (mb_strlen($txt, 'UTF-8') < lire_config('minus/taille_min', 0)) {
		return false;
	}
	$prop = lire_config('minus/prop', 30);
	$prop = $prop/100;
	return (minus_prop_maj($txt) >= $prop);
}

/**
 * Calcule la proportion de minuscule
 * @param string $txt
 * @return float
**/
function minus_prop_maj(string $txt): float {
	if (mb_strlen($txt, 'UTF-8') > 0){
		$nb_maj = levenshtein(mb_strtolower($txt, 'UTF-8'), $txt);
		return $nb_maj/mb_strlen($txt, 'UTF-8');
	}
	return 0;
}

