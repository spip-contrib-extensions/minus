<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Saisies du formulaire de conf
**/
function formulaires_configurer_minus_saisies(): array{
	$flux = [
		[
			'saisie' => 'input',
			'options'=> [
				'nom' => 'taille_min',
				'label' => _T('minus:configurer_taille_min_label'),
				'defaut' => 0
			],
			'verifier' => [
				'type' => 'entier',
				'options' => [
					'min' => 0
				]
			]
		],
		[
			'saisie' => 'input',
			'options'=> [
				'nom' => 'prop',
				'label' => _T('minus:configurer_prop_label'),
				'defaut' => 30
			],
			'verifier' => [
				'type' => 'entier',
				'options' => [
					'min' => 0,
					'max' => 100
				]
			]
		],
	];

	return $flux;
}
