<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/minus.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer' => 'Configurer le plugin Minus',
	'configurer_prop_label' => 'Le pourcentage de majuscules doit être strictement inférieur à',
	'configurer_taille_min_label' => 'Ne pas vérifier si taille strictement inférieure',

	// L
	'label_trop_majuscule' => 'Votre libellé contient beaucoup de majuscules. Êtes vous sûr de ne pas vouloir les mettre en minuscules ? Sur Internet, les majuscules donnent l’impression que l’on crie : utilisez-les avec parcimonie. De plus, normalement votre site distingue visuellement les libellées.',

	// T
	'titre_trop_majuscule' => 'Votre titre contient beaucoup de majuscules. Êtes vous sûr de ne pas vouloir les mettre en minuscules ? Sur Internet, les majuscules donnent l’impression que l’on crie : utilisez-les avec parcimonie.'
);
