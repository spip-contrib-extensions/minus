# Changelog

## 4.0.4 - 2024-05-07

### Fixed

- Compatiblité spip 4.y.z
## 4.0.3 - 2023-03-20

### Changed

- Nécessite explicitement l'extension PHP mbstring

## 4.0.2 - 2023-02-27

### Fixed

- Compatibilité SPIP 4.2
